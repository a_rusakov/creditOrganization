# -*- coding: utf-8 -*-

from django.db import models


''' 
  Информационная модель .
'''


class CustomerProfile(models.Model):
    '''
      Анкета клиента.
    '''
    date_create = models.DateTimeField("Дата и время создания", auto_now_add=True)
    date_change = models.DateTimeField("Дата и время изменения", auto_now_add=True)
    full_name = models.CharField("ФИО клиента", max_length=100)
    dop = models.DateField("Дата рождения", null=True)
    phone = models.CharField("Телефон", max_length=11, null=True)
    pasport_number = models.CharField("Номер паспорта", max_length=11, null=True)
    scoring = models.CharField("Скоринговый бал", max_length=4, null=True)
    #partner = models.ForeignKey("Партнер создавший анкету")


class Offering(models.Model):
    '''
      Предложение.
    '''
    date_create = models.DateTimeField("Дата и время создания", auto_now_add=True)
    date_change = models.DateTimeField("Дата и время изменения", auto_now_add=True)
    date_rotation_begin = models.DateTimeField("Дата и время начала ротации", auto_now_add=True)
    date_rotation_end = models.DateTimeField("Дата и время окончания ротации", auto_now_add=True)
    name = models.CharField("Название предложения", max_length=50)
    type_offering = models.CharField("Тип предложения", max_length=50)
    scoring_min = models.CharField("Минимальный скоринговый бал ", max_length=4, null=True)
    scoring_max = models.CharField("Максимальный скоринговый бал", max_length=4, null=True)
    #credit_organization = models.ForeignKey("Кредитная организация")


class Bid(models.Model):
    '''
      Заявка	в	кредитную	организацию.
    '''
    date_create = models.DateTimeField("Дата и время создания", auto_now_add=True)
    date_dispatch = models.DateTimeField("Дата и время отправки", auto_now_add=True)    
    customer = models.ForeignKey("CustomerProfile", on_delete=models.CASCADE)
    offering = models.ForeignKey("Offering", on_delete=models.CASCADE)
    state = models.CharField("Статаус", max_length=20)


