# -*- coding: utf-8 -*-

from django.contrib import admin

from api.models import CustomerProfile
from api.models import Offering
from api.models import Bid


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('pk', 'full_name', 'date_create', 'date_change', 'dop', 'phone', 'pasport_number', 'scoring')
    list_display_links = ('pk', 'full_name')
    list_filter = ('date_create', 'date_change')
    search_fields = ('full_name', 'phone', 'pasport_number')
    
admin.site.register(CustomerProfile, CustomerAdmin)


class OfferingAdmin(admin.ModelAdmin):
    list_display = ('pk', 'date_create', 'date_change', 'date_rotation_begin', 'date_rotation_end', 
                    'name', 'type_offering', 'scoring_max', 'scoring_min')
                                                                # , 'credit_organization'    
    list_display_links = ['pk', 'name'] 
    list_filter = ('date_create', 'date_change', 'date_rotation_begin', 'date_rotation_end')
    search_fields = ('name', 'type_offering')
                                                                   
admin.site.register(Offering, OfferingAdmin)


class BidAdmin(admin.ModelAdmin):
    list_display = ('pk', 'date_create', 'date_dispatch', 'customer', 'offering', 'state')    
    list_display_links = ['pk', 'customer', 'offering']
    list_filter = ('date_create', 'date_dispatch')
    search_fields = ('state', )
    
admin.site.register(Bid, BidAdmin)



