# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from api.models import CustomerProfile
from api.serializers import CustomerProfileSerializer
from api.models import Bid
from api.serializers import BidSerializer


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def customer_list(request):
    
    #permission_classes = (IsAuthenticated,) 
    #authentication_classes = (TokenAuthentication,)

    #print "is_authenticated = ", request.user.is_authenticated()
    #print "username = ", request.user.username

    #if not request.user.is_authenticated():
    #  return JSONResponse([{"http_state": "401"}], status=401)

    if request.method == 'GET':
        customer = CustomerProfile.objects.all()
        serializer = CustomerProfileSerializer(customer, many=True)
        return JSONResponse(serializer.data, status=201)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CustomerProfileSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def customer_detail(request, pk):
    try:
        item = CustomerProfile.objects.get(pk=pk)
    except CustomerProfile.DoesNotExist:
        return JSONResponse([], status=404)

    if request.method == 'GET':
        serializer = CustomerProfileSerializer(item)
        return JSONResponse(serializer.data, status=201)
    
    if request.method == 'DELETE':
        item.delete()
        return JSONResponse([], status=201)    


@csrf_exempt
def bid_list(request):
    if request.method == 'GET':
        bid = Bid.objects.all()
        serializer = BidSerializer(bid, many=True)
        return JSONResponse(serializer.data, status=201)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = BidSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def bid_detail(request, pk):
    try:
        item = Bid.objects.get(pk=pk)
    except Bid.DoesNotExist:
        return JSONResponse([], status=404)

    if request.method == 'GET':
        serializer = BidSerializer(item)
        return JSONResponse(serializer.data, status=201)

    if request.method == 'DELETE':
        item.delete()
        return JSONResponse([], status=201)

