# -*- coding: utf-8 -*-

from rest_framework import serializers
from api.models import CustomerProfile
from api.models import Offering
from api.models import Bid


class CustomerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerProfile
        fields = ('date_create', 'date_change', 'full_name', 'dop', 'phone', 'pasport_number', 'scoring')
  
    def create(self, validated_data):
        return CustomerProfile.objects.create(**validated_data)      


class OfferingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offering
        fields = ('date_create', 'date_change', 'date_rotation_begin', 'date_rotation_end', 
                  'name', 'type_offering', 'scoring_max', 'scoring_min', 'credit_organization')
  
    def create(self, validated_data):
        return Offering.objects.create(**validated_data)      


class BidSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bid
        fields = ('date_create', 'date_dispatch', 'customer', 'offering', 'state')
  
    def create(self, validated_data):
        return Bid.objects.create(**validated_data)      


